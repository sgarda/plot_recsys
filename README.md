# LOCATION / TIME EMBEDDINGS

In this filder you find the scripts to run the experiments for

"Book Recommendations Beyond the Usual Suspects: Jointly Embedding Book Plots Together with Place and Time Information"

## DATA AND RESOURCES

In order to run the experiments you need first to download the following data sets and resources and unzip them:

- Book summaries : http://www.cs.cmu.edu/~dbamman/booksummaries.html
- Glove pre-trained embeddings (Wikipedia 2014 + Gigawords 5) : https://nlp.stanford.edu/projects/glove/
- (wikipedia film dataset)

## CREATE WORKING ENVIRONMENT

	python create_exp_data.py

Now you can create all the files necessary for the experiments. This is done as a separate step since the entire data set is going to be parsed by the Name Entity recognition model of spacy. This operation is quite
expensive and you want to run it just once.
When generating the embeddings matrices all the words that are present in the data set but for which there is not a pretrained embeddings will be set to be vectors of 0s.

The scripts expects the following options:

	"-i","--input" : the data set. Use `books` if you are using the CMU data set `movies` otherwise
	"-w", "--work-dir" : specify a path were all the files will be stored
	"-f","--file" : specify path to the data set you are using. E.g. "booksummaries.txt"
	"-e","--emb" : specify path to pre-trained word embeddings. E.g. "glove.6B.300d.txt"
	"-m", "--min-count" : Location and Time vocabulary that appears less than the amount specified are going to be ignored
	"--create-input" : subfolder name where the parsed data set will be stored
	"--create-vocab" : subfolder name where the vocabulary files will be stored
	"--create-matrices" : subfolder where the pre-trained embeddings matrices will be stored

At the end of the process you will have a directory that look like this:

./books_wk
 * book_data/
   * summaries.json
 * book300d/
   * loc.npy
   * non-loc.npy
   * time.npy
 * min20_book_vocab/
     * non-loc.vocab
     * time.vocab
     * time.w2i
     * loc.vocab
     * loc.w2i


## EXPERIMENTS

	python loc_emb.py --input book --data-path ./books_wk/book_data/summaries.json --vocab-path ./books_wk/min20_book_vocab --embd-path ./books_ek/book300d


This is the basic script call in order to load all the data in the working environment folder. In order to run the experiments add one of the arguments reported below.

### FIX PLOT - CHANGE TIME / LOCATION

	--exp-fixothers --dim-type loc --dim  portugal (e.g.)

	--exp-fixothers --dim-type time --dim 1945 (e.g.) 


At the moment this experiment runs only with book data.
With this options it is possible to generate - for the books found in the variable `TEST_BOOK_FIXOTHER` -  k (with k set by --top-k) recommendations that should present a similar plot to the selected books but are set in a different time/location. This is achieve by summing the plot embeddings (mean of word vectors that are nor location nor time words) and the pre-trained embedding for the specified location/time.

## VISUALIZE EMEBDDINGS

	--exp-viz books_viz

This will create two files:
- books_viz.metadata
- books_viz.vectors

You can use theese two files with the embeddings projector offered by tensorflow at : http://projector.tensorflow.org/
The file with the extension `.metadata` is used to add metadata to the embeddings to be visualized. If no other options are passed this will store the plot embeddings. 
If you instead use as well: 
	
	--img-plot

this will add the the embeddings to visualize both the embeddings of the book summed with its original location/time embeddings and the book with the hypothetical new location/time specified with `--dim` (default being [loc -  'germany']) for the books/movies in the variable `IMG_BOOK`.

## MIX PLOTS 

	--exp-mix

This experiment is to show that it is possible to mix two different plot embeddings in order to find reccomendation that share properities of both. By default 10 books present as genres "Romance novel" and 10 books with "Crime fiction".
Afterwards the two plots embeddings are averaged and the --top-k most similar books are printed.

## EVALUATE RECOMENDATIONS PLOT EMBEDDINGS

	-- evaluate <n>

This will create a test set of <n> examples. For each examples `--top-k` recomendations are found with the plot embeddings and the baseline KNN trained on unigram features. 
Then the average WordNet Similarity Score is computed for the first recommendation and for all recommendations for the BoW model, the plot embeddings model and the Mixed Model.
The Mixed Model uses BoW predictions except when the overlap score between the recommendation and the test case is below a certain threshold `t`. This threshold is found automatically taking the 20th lowest overlap score of the <n> test cases.

The WordNet Semantic Similarity score is defined as: 


![equation](http://latex.codecogs.com/gif.latex?sim%28book%2Crec%29%20%3D%20%5Cfrac%7B1%7D%7B2%7D%28%5Cfrac%7B%5Csum_%7Bw%20%5Cin%20book%7Dmaxsim%28w%2Cbook%29%7D%7B%5Csum_%7Bw%7D%7D&plus;%20%5Cfrac%7B%5Csum_%7Bw%20%5Cin%20rec%7Dmaxsim%28w%2Crec%29%7D%7B%5Csum_%7Bw%7D%7D%29)


where `maxsim` is defined being the maximum score of the path similarity defined in WordNet.

## COMPARISON BoW - PLOT EMBEDDINGS

	--exp-plot <n>

This will pick randomly <n> books and create a plot that shows the plot embeddings and BoW representation performances based on the WordNet Semantic Similarity score with respect to the an overlap score between the book and the reccomendation defined as:


![equation](http://latex.codecogs.com/gif.latex?%5Cfrac%7BV_%7Bbook%7D%20%5Ccap%20V_%7Brec%7D%7D%7BV_%7Bbook%7D%7D)


## REQUIREMENTS

- Python >= 3
- spacy >= 2.0.3
- pandas >= 0.22
- scikit-learn >= 0.19.1
- nltk >= 3.2.5
- nltk books (use nltk.download())

## REFERENCES

Mihalcea et al. in the paper "Corpus-based and Knowledge-based Measures of Text Semantic Similarity"




 





	








