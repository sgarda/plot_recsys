#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 14 10:55:31 2018

@author: Samuele Garda
"""

import argparse
import logging
import pickle
import json
import numpy as np
import pandas as pd
from create_exp_data import create_dir,join_path
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neighbors import NearestNeighbors 
import random
from semsim_score import sym_sent_sim



logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO') 


def parse_args():
  
  parser = argparse.ArgumentParser(description='Experiment with location in document embeddings')
  parser.add_argument("-i","--input",choices = ["books","movies"],default = 'books', help = "Type of dataset")
  parser.add_argument("--data-path",required = True, help = "Path to dir to JSON with parsed input")
  parser.add_argument("--vocab-path",required = True, help = "Path to dir to folder with vocabulary files")
  parser.add_argument("--embd-path",required = True, help = "Path to dir to folder with emeddings matrices")
  parser.add_argument("--top-k", type = int, default = 10, help = "Top `k` most similar")
  parser.add_argument("--dim", default = "germany",type = str, help = "Add new dimension to plot. Can be either a year or a location. Active only within `--exp-fixothers`. ")
  parser.add_argument("--dim-type", default = 'loc', choices = ('loc','time'),  help = "Type of information to add to the plot")
  parser.add_argument("--exp-fixothers", action = 'store_true', help = "Perform experiment with location change")
  parser.add_argument("--exp-viz", default = False, help = "Write data for visualization of embedding space")
  parser.add_argument("--exp-mix", action = "store_true", help = "Mix plot embeddings")
  parser.add_argument("--exp-plot", default = 0, type = int, help = "Plot BoW vs W2V scores w.r.t. overlap score")
  parser.add_argument("--evaluate", default = 0, type = int, help = "Evaluate method against BoW baseline.")
  parser.add_argument("--img-plot", action = 'store_true', help = "Add to the embeddings visualization imaginary books with new dimension")

  return parser.parse_args()



def get_bow_baseline(texts,k):
  """
  Create baseline KNN.
  
  :params:
    texts (list) : list of list of tokens
    k (int) : number of neighbours. Used afterwards for recommendations
    
  :return:
    clf (sklearn.neighbors.NearestNeighbors) : KNN
    X (scipy.sparse.csr_matrix) : Unigram matrix
  
  """
          
  vect = TfidfVectorizer(preprocessor = ' '.join, tokenizer = str.split, ngram_range = (1,1), use_idf = False)

  X = vect.fit_transform(texts)
  
  clf = NearestNeighbors(n_neighbors=k)
  
  logger.info("Fitting KNN to non location data")
  
  clf.fit(X)
  
  logger.info("Find `{}` recommendation with KNN".format(k-1))
  
  return clf,X
    
def single_book_qual_anal(data,book_idx,rec_indices, pred):
  """
  Print recommendation for a given item. Compute Average semantic similarity score on recommendations.
  
  :params:
    data (pd.DataFrame) : data set with columns `title` and `genre`
    book_idx (int) : index of test case
    rec_indices (list) : indices of recommendations
    
  """
  rec_indices = rec_indices[1:]
    
  recomnedations = [{rec_idx : [data.title[rec_idx], data.genre[rec_idx]]} for rec_idx in rec_indices]
  logger.info("Recomendations:\n{}".format(recomnedations))
  
  rec_simscore = [sym_sent_sim(data['nonloc_tok'][book_idx],data['nonloc_tok'][rec_idx]) for rec_idx in rec_indices]
  logger.info("Average simscore : {}".format(np.mean(rec_simscore)))
  

def write_embd_viz(vocab,w2i,embd_matrix,out_emb,out_metadata):
  """
  Write in a file for visualization embeddings from a vocabulary.
  Used to write either `location` or `time` embeddings.
  
  :params:
    vocab (set) : set of words
    w2i (dict) : lookup table words -> indices in matrix
    embd_matrix (np.ndarray) : embeddings matrix
    out_emb (file) : file where to write embeddings vectors
    out_metadata (file) : file where to write corresponding items to embeddings vectors
  
  """

  for word in vocab:
    out_metadata.write("dimension\t{}\n".format(word))
    vector = '\t'.join(str(x) for x in embd_matrix[w2i[word]])
    out_emb.write("{}\n".format(vector))
      
def load_wk_env_nonloc(data_path,vocab_path,embd_path):
  """
  Wrapper function to load data from experiments evironment : NON-LOC DATA.
  
  :params:
    data_path (str) : path to JSON file where parsed data is stored
    vocab_path (str) : path to PICKLE files where vocabularies are stored
    embd_path (str) : path to NPY matrices
    
  :return:
    other_vocab (set) : plot vocabulary
    oter_w2i (dict) : plot lookup table 
    oth_emb_matrix (np.ndarray) : plot embeddings
    
  """
  
  logger.info("Loading nonloc vocabulary files from `{}`".format(args.vocab_path))
    
  other_vocab = load_pickle(join_path([args.vocab_path, "non-loc.vocab"]))
  
  other_w2i = load_pickle(join_path([args.vocab_path, "non-loc.w2i"]))
      
  logger.info("Loading nonloc embedding matrix from `{}`".format(args.embd_path))
    
  oth_emb_matrix = np.load(join_path([args.embd_path, "non-loc.npy"]))
  
  return other_vocab,other_w2i,oth_emb_matrix
  
      
def load_wk_env_loc(data_path,vocab_path,embd_path):
  """
  Wrapper function to load data from experiments evironment : LOC DATA.
  
  :params:
    data_path (str) : path to JSON file where parsed data is stored
    vocab_path (str) : path to PICKLE files where vocabularies are stored
    embd_path (str) : path to NPY matrices
    
  :return:
    locs_vocab (set) : location vocabulary
    locs_w2i (dict) : location lookup table 
    locs_emb_matrix (np.ndarray) : location embeddings

  """
  
  logger.info("Loading location vocabulary files from `{}`".format(args.vocab_path))
  
  locs_vocab = load_pickle(join_path([args.vocab_path, "loc.vocab"]))
      
  locs_w2i = load_pickle(join_path([args.vocab_path, "loc.w2i"]))
    
  logger.info("Loading location embedding matrix from `{}`".format(args.embd_path))
  
  locs_emb_matrix = np.load(join_path([args.embd_path, "loc.npy"]))
  
  return locs_vocab,locs_w2i,locs_emb_matrix

def load_wk_env_time(data_path,vocab_path,embd_path):
  """
  Wrapper function to load data from experiments evironment : TIME DATA.
   
   
  :params:
    data_path (str) : path to JSON file where parsed data is stored
    vocab_path (str) : path to PICKLE files where vocabularies are stored
    embd_path (str) : path to NPY matrices
    
  :return:
    time_vocab (set) : time vocabulary
    time_w2i (dict) : time lookup table 
    time_emb_matrix (np.ndarray) : time embeddings
  """
  
  logger.info("Loading time vocabulary files from `{}`".format(args.vocab_path))
  
  time_vocab = load_pickle(join_path([args.vocab_path, "time.vocab"]))
      
  time_w2i = load_pickle(join_path([args.vocab_path, "time.w2i"]))
    
  logger.info("Loading time embedding matrix from `{}`".format(args.embd_path))
  
  time_emb_matrix = np.load(join_path([args.embd_path, "time.npy"]))
  
  return time_vocab,time_w2i,time_emb_matrix
  

def get_overlap_score(book, single_recom):
  """
  Compute vocabulary overlapping score.
  
  :math:`\\frac{V_{book} \\cap V_{rec}}{V_{book}}`
  
  :params:
    book (list) : list of tokens
    single_recom (list) : list of tokens
  
  :return:
    overalp_score (int) : weighted amount of overlapping words
  
  """
  
  overlap_score = len(set(book).intersection(set(single_recom)))/len(set(book))
  
  return overlap_score

def get_overlap(data,test_idx_data,top_k_idx,mode = 'threshold', t = 20):
  """
  Finde threshold for mixed system (BoW + W2V).
  
  :params:
    data (pandas.Dataframe) : dataset
    test_idx_data (list) : list of indices (test cases)
    top_k_idx (list) : list of list of recomendations
    t (int) : nth overlapping score to be used as threshold
    
  :return:
    threshold (int) : overlapping score to be used to decide for BoW or W2V vectors
  """
  
  scores = []
  
  for idx,test_idx in enumerate(test_idx_data):
    
    ols = [get_overlap_score(data['nonloc_tok'][test_idx],data['nonloc_tok'][top_k_idx[idx][i]]) for i in range(len(top_k_idx[idx]))]
    scores.append(ols)
    
  sorted_scores = sorted([score for sublist in scores for score in sublist])
    
  if mode == 'threshold':
    
    return sorted_scores[t]
    
  elif mode == 'scores':
    
    return sorted_scores


def get_mixed_model(bow_top_k_idx, w2v_top_k_idx, idx_test_data,threshold, k):
  """
  Create recomendations with mixed models. If overalp score is below threshold W2V model is used else BoW.
  
  :params:
    bow_top_k_idx (list) : list of list of recomendations (n recomendations for each test case)
    w2v_top_k_idx (list) : list of list of recomendations (n recomendations for each test case)
    idx_test_data (list) : list of test cases (indices)
    threshold (int) : if overlap score below this value W2V model is used
    k (int) : # of recomendations
  
  :return:
    mixed_top_k_idx (list) : list of list of recomendations
    
  """
  
  mixed_top_k_idx = []
      
  for idx,test_idx in enumerate(idx_test_data):
    single_rec_idx = []
    for i in range(k):
      ol_score = get_overlap_score(data['nonloc_tok'][test_idx],data['nonloc_tok'][bow_top_k_idx[idx][i]])
      if ol_score <= threshold:
        rec = w2v_top_k_idx[idx][0]
      else:
        rec = bow_top_k_idx[idx][i]
      single_rec_idx.append(rec)
    mixed_top_k_idx.append(single_rec_idx)
  
  return mixed_top_k_idx

def cosine_similarity(vector,matrix):
  """
  Compute cosine similarity of vector with each vector belonging to matrix.
  
  :math:`cos(x,y)=\\frac{x \cdot y}{||x|| \cdot ||y||}`
  
  :params:
    vector (np.ndarray) : vector
    matrix (np.ndarray) : matrix
    
  :return:
    similarites (np.ndarray) : vector containing cosine similarities \
    of input vector with each row in matrix
  """
  
  
  norm = np.linalg.norm(vector)
  all_norms = np.linalg.norm(matrix, axis=1)
  dot_products = np.dot(matrix, vector)
  similarities = dot_products / (norm * all_norms)
  
  return similarities


def load_json_data(data_path):
  """
  Load JSON file (one item per line)
  
  :params:
    data_path (str) : path to JSON file
    
  :return:
    
    data (pandas.DataFrame) : DataFrame with columns being JSON fields
  """
  
  data = []
  
  with open(data_path) as infile:
    for line in infile:
      file = json.loads(line)
      data.append(file)
      
  return pd.DataFrame(data)


def sent_to_indices(sent, w2i):
  """
  Map list of tokens to their ids
  
  :params:
    sent (str) : sentence
    w2i (dict) : dictionary mapping tokens to id
    
  :return:
    list of ids
  """
  
  return [w2i.get(w,w2i['UNK']) for w in sent]


def load_pickle(file_path):
  """
  Load item saved in pickle file.
  
  :params:
    file_path (str) : path to pickle file
  :return:
    item : content of file
    
  """
  item = pickle.load(open(file_path, mode = "rb"))
  return item

if __name__ == "__main__":
  
  random.seed(42)
  np.random.seed(42)
  
  args = parse_args()

  TEST_BOOK_FIXOTHER  =   ["Adventures of Huckleberry Finn","A Farewell to Arms","Oliver Twist","Nineteen Eighty-Four"]
  
#                          "The Body","The Great Gatsby","Murder on the Orient Express",
#                           "The Three Musketeers","On the Genealogy of Morals","Nineteen Eighty-Four",
#                           "War and Peace","The Twilight of the Idols","Murder on the Links",
#                           "Dracula","Harry Potter and the Deathly Hallows","Harry Potter and the Order of the Phoenix",
#                           "Hamlet","Macbeth","The Hound of the Baskervilles",
#                           "The Man with the Twisted Lip","The Brothers Karamazov","Anna Karenina",
#                           "The Sorrows of Young Werther","Doktor Faustus" ]      

  IMG_BOOK = ["The Charioteer of Delphi", "The Dragon Lord", "The Whiskey Rebels","The Gladiators from Capua"]
  
  VIZ_MOVIES = ["en Star Wars: Episode II   Attack of the Clones\n","en Star Wars: Episode III   Revenge of the Sith\n",
                "en Harry Potter and the Goblet of Fire (film)\n","en Harry Potter and the Chamber of Secrets (film)\n",
                "en The Lord of the Rings: The Fellowship of the Ring\n","en The Lord of the Rings: The Two Towers\n",
                "en Fast & Furious (2009 film)\n","en Fast & Furious 6\n",
                "en The Ring (2002 film)\n", "en The Ring Two\n"]  
  

  VIZ_BOOKS = ["Oliver Twist","Dracula","Harry Potter and the Deathly Hallows","Harry Potter and the Order of the Phoenix", "Hamlet","Macbeth",
                "Adventures of Huckleberry Finn","The Adventures of Tom Sawyer", "The Hound of the Baskervilles","The Man with the Twisted Lip",
                "War and Peace","The Brothers Karamazov","The Devils","Anna Karenina","The Sorrows of Young Werther",
                "Doktor Faustus","The Twilight of the Idols","Murder on the Links" ]
  
  
  VIZ = VIZ_BOOKS if args.input == 'books' else VIZ_MOVIES



  TEST_DIM = args.dim
  
  
  k = args.top_k + 1
  

  logger.info("Loading processed summaries from `{}`".format(args.data_path))
  
  data = load_json_data(args.data_path)
    
  
  docs_others = list(data.nonloc_tok)
  
  other_vocab,other_w2i,oth_emb_matrix = load_wk_env_nonloc(args.data_path,args.vocab_path,args.embd_path)

  X_others = [sent_to_indices(sent,other_w2i) for sent in docs_others]
  
  
  docs_locs = list(data.loc_tok)
  
  locs_vocab,locs_w2i,locs_emb_matrix = load_wk_env_loc(args.data_path,args.vocab_path,args.embd_path)
  
  X_locs = [sent_to_indices(sent,locs_w2i) for sent in docs_locs]
  
  
  docs_time = list(data.time_tok)
  
  time_vocab,time_w2i,time_emb_matrix = load_wk_env_time(args.data_path,args.vocab_path,args.embd_path)
  
  X_times = [sent_to_indices(sent,time_w2i) for sent in docs_time]
        
  
  if args.exp_viz:
    
    logger.info("Creating files for visualize embeddings with : `http://projector.tensorflow.org/`")
      
    create_dir(args.exp_viz)
    
    logger.info("Creating files for visualization at `{}`".format(args.exp_viz))
    
    out_emb = open("{}.vectors".format(join_path([args.exp_viz,args.exp_viz])), "w")
    out_metadata = open("{}.metadata".format(join_path([args.exp_viz,args.exp_viz])), "w")
    
    out_metadata.write("type\ttitle\n")
    
#    write_embd_viz(locs_vocab,locs_w2i,locs_emb_matrix,out_emb,out_metadata)
    
#    write_embd_viz(other_vocab,other_w2i,oth_emb_matrix,out_emb,out_metadata)
  
  Ws_locs = []
  
  Ws_others = []
  
  Ws_times = []
  
  to_drop = []
  
  
  for idx,(X_other,X_loc,X_time) in enumerate(zip(X_others,X_locs,X_times)):
    
    if X_other and X_loc and X_time:
      
      W_others = np.mean(oth_emb_matrix[X_other], axis = 0)
     
      W_times = np.mean(time_emb_matrix[X_time], axis = 0)
     
      W_locs = np.mean(locs_emb_matrix[X_loc],axis = 0)
    
    
      if args.dim_type == 'time':
      
        W_others = np.mean([W_others,W_locs], axis = 0)
                  
        Ws_others.append(W_others[np.newaxis,:])
        
        Ws_times.append(W_times[np.newaxis,:])  
      
      
      elif args.dim_type == 'loc':
  
        W_others = np.mean([W_others,W_times], axis = 0)
                  
        Ws_others.append(W_others[np.newaxis,:])
        
        Ws_locs.append(W_locs[np.newaxis,:])
        
    else:
        
      to_drop.append(idx)

    if args.exp_viz:
      
      if data.title[idx] in VIZ:
        
        out_metadata.write("plot-book\t{}\n".format(data.title[idx]+'_plot'))
        out_emb.write("{}\n".format('\t'.join(str(x) for x in W_others)))
        
      
      if args.input == 'books':
        
        if args.img_plot:
          
          if data.title[idx] in IMG_BOOK:
        
            out_metadata.write("plot-book\t{}\n".format(data.title[idx]+'_plot'))
            out_emb.write("{}\n".format('\t'.join(str(x) for x in W_others)))
      
            D = np.sum([W_locs,W_others], axis = 0)
        
            out_metadata.write("book+dim\t{}\n".format(data.title[idx]))
            out_emb.write("{}\n".format('\t'.join(str(x) for x in D)))
            
            new_dim = locs_emb_matrix[locs_w2i[TEST_DIM]] if args.dim_type == 'loc' else time_emb_matrix[time_w2i[TEST_DIM]]
            
            test_new_book = np.sum([W_others, new_dim ], axis = 0)
            out_metadata.write("img-book\t{}\n".format(data.title[idx]+'_{}'.format(TEST_DIM)))
            out_emb.write("{}\n".format('\t'.join(str(x) for x in test_new_book)))
          
      else:
        
        raise ValueError("Immaginary plot visualization for movies is not yet available!")
      
  logger.info("Dropped {} items missing location or time tokens".format(len(to_drop)))
  
  data = data.drop(data.index[[to_drop]])
    
  data.index = range(len(data))
          
  
  if args.evaluate != 0:
    
    logger.info("Start evaluation process on {} cases".format(args.evaluate))
    
    texts = list(data['nonloc_tok'])
        
    idx_test_data = np.random.randint(low = 0, high = len(data), size = args.evaluate)

    clf,X = get_bow_baseline(texts,k)
    
    top_k_idx_bow = [clf.kneighbors(X[idx_test],return_distance = False)[0] for idx_test in idx_test_data]

    Ws_others = np.concatenate(Ws_others, axis = 0)
  
    logger.info("Find `{}` recommendation with W2V".format(k-1))
    
    sims_noloc = [cosine_similarity(Ws_others[idx_test],Ws_others) for idx_test in idx_test_data]
      
    top_k_idx_w2v =  [np.argpartition(-sim_noloc.ravel(),range(k), axis = 0)[:k] for sim_noloc in sims_noloc]
    
    overlap_threshold = get_overlap(data,idx_test_data,top_k_idx_bow,mode = 'threshold', t = 20)
    
    logger.info("Overlapping threshold for Mixed model is {}".format(overlap_threshold))
    
    top_k_idx_mixed = get_mixed_model(top_k_idx_bow, top_k_idx_w2v, idx_test_data,overlap_threshold, k)
    
    logger.info("Computing WordNet Semantic Similarity Score")
    
    first_hit_scores_w2i = []
    first_hit_scores_bow = []
    first_hit_scores_mixed = []
    average_10_hits_w2i = []
    average_10_hits_bow = []
    average_10_hits_mixed = []
    
    for idx,test_idx in enumerate(idx_test_data):
      
      w2i_scores = [sym_sent_sim(data['nonloc_tok'][test_idx],data['nonloc_tok'][top_k_idx_w2v[idx][i]]) for i in range(1,k)]
            
      bow_scores =  [sym_sent_sim(data['nonloc_tok'][test_idx],data['nonloc_tok'][top_k_idx_bow[idx][i]]) for i in range(1,k)]
      
      mixed_scores = [sym_sent_sim(data['nonloc_tok'][test_idx],data['nonloc_tok'][top_k_idx_mixed[idx][i]]) for i in range(1,k)]
      
      if (idx+1) % 10 == 0:
        logger.info("Processed {} test case".format(idx+1))
      
      logger.info("W2V : {}".format(w2i_scores))
      logger.info("BoW : {}".format(bow_scores))
            
      first_hit_scores_w2i.append(w2i_scores[0])
      first_hit_scores_bow.append(bow_scores[0])
      first_hit_scores_mixed.append(mixed_scores[0])
      average_10_hits_w2i.append(np.mean(w2i_scores))
      average_10_hits_bow.append(np.mean(bow_scores))
      average_10_hits_mixed.append(np.mean(mixed_scores))
      
    
    logger.info("Average first hit similarity score for W2V on {} book summaries : {}".format(args.evaluate, np.mean(first_hit_scores_w2i)))
    logger.info("Average first hit similarity score for BoW on {} book summaries : {}".format(args.evaluate, np.mean(first_hit_scores_bow)))
    logger.info("Average first hit similarity score for Mixed on {} book summaries : {}".format(args.evaluate, np.mean(first_hit_scores_mixed)))
    logger.info("Average 10 hits similarity score for W2V on {} book summaries : {}".format(args.evaluate, np.mean(average_10_hits_w2i)))
    logger.info("Average 10 hits similarity score for BoW on {} book summaries : {}".format(args.evaluate, np.mean(average_10_hits_bow)))
    logger.info("Average 10 hits similarity score for Mixed on {} book summaries : {}".format(args.evaluate, np.mean(average_10_hits_mixed)))
    
    
  elif args.exp_plot:
    
    idx_test_data = np.random.randint(low = 0, high = len(data), size = args.exp_plot)
      
    texts = list(data['nonloc_tok'])
    
    clf,X = get_bow_baseline(texts,k)
    
    top_k_idx_bow = [clf.kneighbors(X[idx_test],return_distance = False)[0] for idx_test in idx_test_data]

    Ws_others = np.concatenate(Ws_others, axis = 0)
  
    logger.info("Find `{}` recommendation with W2V".format(k-1))
    
    sims_noloc = [cosine_similarity(Ws_others[idx_test],Ws_others) for idx_test in idx_test_data]
      
    top_k_idx_w2v =  [np.argpartition(-sim_noloc.ravel(),range(k), axis = 0)[:k] for sim_noloc in sims_noloc]
    
    logger.info("Starting creating scores for plot")
  
    simscore_table = {}
    df_index = 0
    for idx,test_idx in enumerate(idx_test_data):
      logger.info("Processing test case : {}".format(idx+1))
      for i in range(1,k):
        simscore_table[df_index] = {}
        simscore_table[df_index]['BoW'] = sym_sent_sim(data['nonloc_tok'][test_idx],data['nonloc_tok'][top_k_idx_bow[idx][i]])
        simscore_table[df_index]['W2V'] = sym_sent_sim(data['nonloc_tok'][test_idx],data['nonloc_tok'][top_k_idx_w2v[idx][i]])
        simscore_table[df_index]['overlap'] = get_overlap_score(data['nonloc_tok'][test_idx],data['nonloc_tok'][top_k_idx_bow[idx][i]])
        df_index += 1
        
    data_score = pd.DataFrame.from_dict(simscore_table, orient='index')
    
    pickle.dump(simscore_table, open('./score_table.dict', 'wb'))
    
    data_score = data_score.sort_values(by = 'overlap')
    
    ax = data_score.plot(x = 'overlap')
    fig = ax.get_figure()
    fig.savefig('./scores_plot_{}t_{}k'.format(args.exp_plot,args.top_k))
  
        
  elif args.exp_fixothers :
    
    if args.input == 'books':

    
      logger.info("Evaluating embeddings arithmetics with new dimension: {}".format(args.dim))
      
      logger.info("Creating W2V models")
      
      if args.dim_type == 'loc':
      
        Ws_locs = np.concatenate(Ws_locs, axis = 0)
        
        Ws_others = np.concatenate(Ws_others, axis = 0)
        
        D = np.sum([Ws_locs,Ws_others],axis = 0)
        
        new_dim = locs_emb_matrix[locs_w2i[TEST_DIM]]
        
      elif args.dim_type == 'time':
        
        Ws_times = np.concatenate(Ws_times, axis = 0)
        
        Ws_others = np.concatenate(Ws_others, axis = 0)
        
        D = np.sum([Ws_times,Ws_others],axis = 0)
        
        new_dim = time_emb_matrix[time_w2i[TEST_DIM]]
        
      
      book_to_test = TEST_BOOK_FIXOTHER 
      
      logger.info("Creating BoW baselines...")
    
      texts_noloc = list(data['nonloc_tok'])
      
      clf_noloc,X_noloc = get_bow_baseline(texts_noloc,k)
      
      logger.info("Start qualitative evaluation process...")
      
      for book in book_to_test:
        
        book_idx = data.index[data.title==book][0]
        
        dim_book = data.loc_tok[book_idx] if args.dim_type == 'loc' else data.time_tok[book_idx]
        
        logger.info("\nAssessing recommendations for:\n\n{} - {} - {}".format(data.title[book_idx], data.genre[book_idx],
              dim_book))
        
  #     BoW recomendatoins indices
        bow_noloc_idx = clf_noloc.kneighbors(X_noloc[book_idx],return_distance = False)[0]
        
              
  #     W2V recomendations indices
        sims_noloc = cosine_similarity(Ws_others[book_idx],Ws_others)
        
        w2v_noloc_idx =  np.argpartition(-sims_noloc.ravel(),range(k), axis = 0)[:k]
        
  #     New Location + W2V recomendations indices
        test_new = np.sum([Ws_others[book_idx], new_dim], axis = 0)
        
        sims_new_D = cosine_similarity(test_new,D)
        
        w2v_new_D =  np.argpartition(-sims_new_D.ravel(),range(k), axis = 0)[:k]
              
        logger.info("\nW2V_loc_D:\n")
        recomnedations = [{rec_idx : [data.title[rec_idx], data.genre[rec_idx]]} for rec_idx in w2v_new_D]
        logger.info("Recomendations:\n{}\n".format(recomnedations))
       
#        logger.info("\nBow_noloc:\n")
#        single_book_qual_anal(data,book_idx,bow_noloc_idx)
#  
#        logger.info("\nW2v_noloc:\n")
#        single_book_qual_anal(data,book_idx,w2v_noloc_idx)
        
        
    else:
      
      raise ValueError("Sorry! Embeddings arithmetics is not available for movies yet!")

  elif args.exp_mix:
    
    if args.input == 'books':
    
      logger.info("Evaluating mixture of plot...")
      
      Ws_others = np.concatenate(Ws_others, axis = 0)
      
      romance_idx = [idx for idx,gen in enumerate(list(data.genre)) if  isinstance(gen,dict) and "Romance novel" in gen.values()]
          
      crime_idx = [idx for idx,gen in enumerate(list(data.genre)) if  isinstance(gen,dict) and "Crime Fiction" in gen.values()]
          
      romance_samples = random.sample(romance_idx, 10)
      
      crime_samples = random.sample(crime_idx, 10)
      
      for rom_s,cri_s in zip(romance_samples,crime_samples):
        
        logger.info("Assessing : {} - {} +\n".format(data.title[rom_s],data.genre[rom_s]))
        logger.info("{} - {}\n".format(data.title[cri_s],data.genre[cri_s]))
        
        if rom_s != cri_s:
        
          romance_plt = Ws_others[rom_s]
          crime_plt = Ws_others[cri_s]
          
          avg = np.mean([romance_plt,crime_plt], axis = 0)
          
          sims_Ws_others = cosine_similarity(avg,Ws_others)
          
          w2v_Ws_others =  np.argpartition(-sims_Ws_others.ravel(),range(k), axis = 0)[:k]
          
          recomnedations = [{rec_idx : [data.title[rec_idx], data.genre[rec_idx]]} for rec_idx in w2v_Ws_others]
          
          logger.info("Recomendations:\n{}\n".format(recomnedations))
          
        
      else:
        pass
      
    else:
      
      raise ValueError("Sorry! Mixing plots is not available for movies!")
      
      
      